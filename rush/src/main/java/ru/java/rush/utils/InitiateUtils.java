package ru.java.rush.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import ru.java.rush.entities.AuthorEntity;
import ru.java.rush.entities.BookEntity;
import ru.java.rush.services.AuthorService;
import ru.java.rush.services.BookService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InitiateUtils implements CommandLineRunner {

    private final BookService bookService;
    private final AuthorService authorService;

    @Override
    public void run(String... args) throws Exception {
        List<BookEntity> bookEntityList = new ArrayList<>(
                Arrays.asList(
                        new BookEntity()
                                .setNameBook("Горе от ума")
                                .setYearCreat(1824)
                                .setAuthorId(1),
                        new BookEntity()
                                .setNameBook("Война и мир")
                                .setYearCreat(1863)
                                .setAuthorId(2),
                        new BookEntity()
                                .setNameBook("Мцыри")
                                .setYearCreat(1838)
                                .setAuthorId(3),
                        new BookEntity()
                                .setNameBook("Евгений Онегин")
                                .setYearCreat(1833)
                                .setAuthorId(4)
                )
        );

        List<AuthorEntity> authorEntityList = new ArrayList<>(
                Arrays.asList(
                        new AuthorEntity()
                                .setFirstNameAuthor("Александр")
                                .setLastNameAuthor("Грибоедов"),
                        new AuthorEntity()
                                .setFirstNameAuthor("Лев")
                                .setLastNameAuthor("Толстой"),
                        new AuthorEntity()
                                .setFirstNameAuthor("Михаил")
                                .setLastNameAuthor("Лермонтов"),
                        new AuthorEntity()
                                .setFirstNameAuthor("Александр")
                                .setLastNameAuthor("Пушкин")
                ));

        bookService.saveAll(bookEntityList);
        authorService.saveAll(authorEntityList);

        System.out.println("\nТаблица книг");
        for(BookEntity book: bookService.findAll()){
            System.out.println(book);
        }

        System.out.println("\nТаблица авторов");
        for(AuthorEntity author : authorService.findAll()){
            System.out.println(author);
        }
    }
}
