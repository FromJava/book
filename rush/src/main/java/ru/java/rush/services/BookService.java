package ru.java.rush.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.java.rush.entities.BookEntity;
import ru.java.rush.repositories.BookRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;

    public void saveAll(List<BookEntity> books) {
        bookRepository.saveAll(books);
    }

    public List<BookEntity> findAll() {
        return bookRepository.findAll();
    }
}
