package ru.java.rush.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.java.rush.entities.AuthorEntity;
import ru.java.rush.repositories.AuthorRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;

    public void saveAll(List<AuthorEntity> books) {
        authorRepository.saveAll(books);
    }

    public List<AuthorEntity> findAll() {
        return authorRepository.findAll();
    }
}
