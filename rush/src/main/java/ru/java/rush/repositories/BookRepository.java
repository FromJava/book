package ru.java.rush.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.java.rush.entities.BookEntity;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Integer> {


}
